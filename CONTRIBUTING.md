koa_example - โครง koa เปล่าที่ใส่ library ทุกตัวครบในวันเสาร์ที่ติว
koa_login_basic_all - ระบบ login ครบทุกอย่างวันพฤหัสที่ผ่านมา
koa_login_basic_form_only - ระบบ login แบบมี form post แล้ว แต่ยังไม่ใส่ session เผื่อใครอยากจะเอาเป็นแบบฝึกหัดไปทำต่อเองจากโครงที่มีให้แค่ฟอร์ม
koa_login_multi_file - ระบบ login ที่เอา koa_login_basic_all มาแยกไฟล์หลายๆ ไฟล์ตามความเหมาะสม