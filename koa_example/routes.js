const Router = require('koa-router');
const router = new Router();
const show = require('./controllers/show.js')
const show2 = require('./controllers/show2.js')

router.get('/', show.home)
.get('/show/:id/:id2', show.showTwoUser)
.get('/show2/:id/:id2', show2.showTwoUser2)
.get('/show3/:id/:id2', show2.showTwoUser3)
.get('/show4/:id/:id2', show2.showTwoUser4)
.get('/show5/:id/:id2', show2.showTwoUser5)
.post('/show_post', show2.showPost)
.get('/form', show2.form)
;

module.exports = router;