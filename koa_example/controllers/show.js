const db = require('../libs/db.js');

module.exports = {
    async home (ctx) {
        let n = ctx.session.views || 0;
        ctx.session.views = ++n;
        ctx.body = n + ' views';
        /*await ctx.render("user", {
            "firstname" : "Somchai",
            "lastname" : "Jaidee"
        });*/
    },
    async showTwoUser (ctx) {
        let rows = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id]);
        let rows2 = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id2]);
        
        if (ctx.session.count == undefined)
            ctx.session.count = 0;
        else
            ctx.session.count++;
    
        await ctx.render("show", {
            "username" : rows[0].username,
            "mobile_no" : rows[0].mobile_no,
            "username2" : rows2[0].username,
            "count" : ctx.session.count
        });
    }
}