const mysql = require('mysql2/promise');

const db  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'koa_login'
});

module.exports = db;