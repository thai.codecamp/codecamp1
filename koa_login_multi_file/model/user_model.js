const db = require('../lib/db');

module.exports = {
    async insertNewUser(username, password, email) {
        let sql = `INSERT INTO user
        (username,password,email)
        VALUES
        (?,?,?)
        `;
        let [result, fields] = await db.query(sql,[username, this.stupidHash(password), email]);
        return result.insertId;
    },
    stupidHash(password) {
        return password + "12345";
    },
    async isEmailExisted(email) {
        let sql = `SELECT email FROM user
        WHERE email = ?`;
        let [result, fields] = await db.query(sql,[email]);
        if (result[0])
            return true;
        else
            return false;
    },
    async isUsernameExisted(username) {
        let sql = `SELECT username FROM user
        WHERE username = ?`;
        let [result, fields] = await db.query(sql,[username]);
        if (result[0])
            return true;
        else
            return false;
    },
    async isPasswordMatched(username, password) {
        let sql = `SELECT password FROM user
        WHERE username = ?`;
        let [result, fields] = await db.query(sql,[username]);
        if (result[0].password == this.stupidHash(password))
            return true;
        else
            return false;
    },
    async getUserInfoByUserId(userId) {
        let sql = `SELECT * FROM user WHERE user_id=?`;
        let [result, fields] = await db.query(sql,[userId]);
        return result[0];
    },
    async getUserInfoByUsername(username) {
        let sql = `SELECT * FROM user WHERE username=?`;
        let [result, fields] = await db.query(sql,[username]);
        return result[0];
    }
}
