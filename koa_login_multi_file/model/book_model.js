const db = require('../lib/db');

module.exports = {
    async insertNewBook(user_id, book_name) {
        let sql = `INSERT INTO book
        (user_id,book_name)
        VALUES
        (?,?)
        `;
        let [result, fields] = await db.query(sql,[user_id, book]);
        return result.insertId;
    }
}
