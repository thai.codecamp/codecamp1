const Koa = require('Koa');
const Router = require('koa-router');
const session = require('koa-session');
const render = require('koa-ejs');
const path = require('path');
const root = '/';
const app = new Koa();
const router = new Router();
//const formidable = require('koa2-formidable');
const bodyParser = require('koa-bodyparser');
const userController = require('./controller/user');
const userModel = require('./model/user_model');

const baseUrl = "http://localhost:3000/";

const serve = require('koa-static');
app.use(serve('public'));
render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.keys = ['asdf;lkdsa naksldflkdsajflkdsa'];
//app.use (formidable ());
app.use(bodyParser());

router.get('/', async(ctx) => {
    ctx.body = "homepage";
})
.get('/register', userController.register)
.post('/register_completed', userController.registerCompleted)
.post('/register_completed_ajax', userController.registerCompletedAjax)
.get('/login', userController.login)
.post('/login_completed', userController.loginCompleted)
.get('/my_profile', userController.myProfile)
.get('/logout', userController.logout)
.get('/admin', userController.admin)
.post('/upload_profile_image',userController.uploadProfileImage);

const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    maxAge: 60 * 60 * 1000,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        }, 
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {

        }
    }
};
  
  app.use(session(CONFIG, app));
  app.use (async (ctx, next) => {
    if (ctx.path == '/login_completed') {
        await next();
        return;
    }

    if ( !ctx.session || 
        (ctx.session && !ctx.session.userId )
    ) {
        await ctx.render('login');
        
    } else {
        let userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
        if (userRow.role != 'admin' && ctx.path == '/admin')
            ctx.body = "Forbidden!!";
        else
            await next();
    }
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);